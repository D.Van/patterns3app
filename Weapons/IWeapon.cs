﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Weapons
{
    internal interface IWeapon
    {
        int Melee();
    }
}
