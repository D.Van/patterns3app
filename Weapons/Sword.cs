﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Weapons
{
    internal class Sword : IWeapon
    {
        public int Melee()
        {
            Random random = new Random();
            int dmg = random.Next(1, 21);
            Console.WriteLine($"Нанесено {dmg} урона мечом");
            return dmg;
        }
    }
}
