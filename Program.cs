﻿using Patterns3app.Enemies;
using Patterns3app.Factories;

namespace Patterns3app
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Horde horde = new Horde();
            Factory factory;
            if (Game.Pick()==1)
            {
                factory = new EasyFactory();
            }
            else
            {
                factory = new HardFactory();
            }
            for (int i = 0; i < 8; i++)
            {
                horde.Add(factory.weakenemy());
                Console.WriteLine($"В бой входит {horde.enemyList[horde.enemyList.Count() - 1].getName()}!");
            }
            for (int i = 0; i < 2; i++)
            {
                horde.Add(factory.strongenemy());
                Console.WriteLine($"В бой входит {horde.enemyList[horde.enemyList.Count() - 1].getName()}!");
            }    
            horde.attack();
            horde.takeDamage(25);
            horde.enemyList[0].attack(Game.weapons[0]);
            horde.enemyList[0].takeDamage(25);
        }
    }
}