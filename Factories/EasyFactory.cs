﻿using Patterns3app.Enemies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Factories
{
    internal class EasyFactory : Factory
    {
        public override IEnemy weakenemy()
        {
            return new Skeleton();
        }
        public override IEnemy strongenemy()
        {
            return new Zombie();
        }
    }
}
