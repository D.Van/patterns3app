﻿using Patterns3app.Enemies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Factories
{
    internal class HardFactory : Factory
    {
        public override IEnemy weakenemy()
        {
            return new Zombie();
        }
        public override IEnemy strongenemy()
        {
            return new Supermutant();
        }
    }
}
