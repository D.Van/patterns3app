﻿using Patterns3app.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Enemies
{
    internal interface IEnemy
    {
        public void setHp(int hp);
        public int getHp();
        public string getName();
        public void setName(string name);
        public int attack(IWeapon weapon);
        public void takeDamage(int dmg);

    }
}
