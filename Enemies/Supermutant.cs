﻿using Patterns3app.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Enemies
{
    internal class Supermutant : IEnemy
    {
        private int hp;
        private string name = "Супермутант";

        public Supermutant()
        {
            hp = 25;
        }
        public int getHp()
        {
            return hp;
        }
        public void setHp(int hp)
        {
            this.hp = hp;
        }
        public string getName()
        {
            return name;
        }
        public void setName(string name)
        {
            this.name += "-" + name;
        }
        public int attack(IWeapon weapon)
        {
            if (weapon != null)
            {
                return weapon.Melee();
            }
            return 0;
        }
        public void takeDamage(int dmg)
        {
            dmg /= 2;
            if (hp > dmg)
            {
                hp -= dmg;
                Console.WriteLine($"Супермутант понёс {dmg} урона!");
            }
            else
                Console.WriteLine("Супермутант убит.");
        }
    }
}
