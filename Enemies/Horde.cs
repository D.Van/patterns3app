﻿using Patterns3app.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app.Enemies
{
    internal class Horde: IEnemy
    {
        public List<IEnemy> enemyList = new List<IEnemy>();
        public void setHp(int hp)
        {
            foreach (IEnemy enemy in enemyList)
            {
                enemy.setHp(hp);
            }
        }
        public int getHp()
        {
            int hp = 0;
            foreach(IEnemy enemy in enemyList)
            {
                hp+=enemy.getHp();
            }
            return hp;
        }
        public string getName()
        {
            return "Орда";
        }
        public void setName(string name)
        {
            Console.WriteLine("У орды есть лишь одно имя.");
        }
        public void Add(IEnemy enemy)
        {
            enemyList.Add(enemy);
            enemy.setName((enemyList.Count - 1).ToString());
        }
        public void Delete(int num)
        {
            enemyList.RemoveAt(num);
        }
        public void attack()
        {
            Random random = new Random();
            foreach (IEnemy enemy in enemyList)
            {
                enemy.attack(Game.weapons[random.Next(0, 3)]);
            }
        }
        public int attack(IWeapon weapon)
        {
            int dmg = 0;
            foreach (IEnemy enemy in enemyList)
            {
                dmg += enemy.attack(weapon);
            }
            return dmg;
        }
        public void takeDamage(int dmg)
        {
            dmg /= enemyList.Count;
            for (int i = 0; i < enemyList.Count; i++)
            {
                if (enemyList[i].getHp() > dmg)
                {
                    enemyList[i].setHp(enemyList[i].getHp() - dmg);
                    Console.WriteLine($"{enemyList[i].getName()} понёс {dmg} урона!");
                }
                else
                {
                    Delete(i);
                    Console.WriteLine($"{enemyList[i].getName()} убит.");
                }
            }
        }
    }
}
