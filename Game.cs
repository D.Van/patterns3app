﻿using Patterns3app.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns3app
{
    internal static class Game
    {
        public static List<IWeapon> weapons = new List<IWeapon>() { new Claws(), new Jaws(), new Sword()};
        public static int Pick()
        {
            Console.WriteLine("Выберите сложность (1-Легкая/2-Сложная).");
            string? command = Console.ReadLine();
            while (command != "1" && command != "2")
            {
                Console.WriteLine("Выберите сложность (1-Легкая/2-Сложная).");
                command = Console.ReadLine();
            }
            return int.Parse(command);
        }
    }
}
